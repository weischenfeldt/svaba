#!/usr/bin/env cwl-runner
class: CommandLineTool
cwlVersion: v1.0

dct:creator:
  '@id': http://orcid.org/0000-0003-3684-2659
  foaf:name: Francesco Favero
  foaf:mbox: francesco.favero@bric.ku.dk

dct:contributor:
  foaf:name: Etsehiwot Girum Girma
  foaf:mbox: Etsehiwot@finsenlab.dk

requirements:
  - class: DockerRequirement
    dockerPull: registry.hub.docker.com/weischenfeldt/svaba_workflow:1.0.0
hints:
  - class: ResourceRequirement
    coresMin:
    ramMin:
    outdirMin:

inputs:
  normal-bam:
    type: File
    inputBinding:
      position: 1
      prefix: --normal-bam
  tumor-bam:
    type: File
    inputBinding:
      position: 2
      prefix: --tumor-bam
  reference-genome:
    type: File
    inputBinding:
      position: 3
      prefix: --reference-genome
  reference-gz-ann:
    type: ["null", File]
    inputBinding:
      position: 4
      prefix: --reference-gz-ann
  reference-gz-amb:
    type: ["null", File]
    inputBinding:
      position: 5
      prefix: --reference-gz-amb
  reference-gz-bwt:
    type: ["null", File]
    inputBinding:
      position: 6
      prefix: --reference-gz-bwt
  reference-gz-pac:
    type: ["null", File]
    inputBinding:
      position: 7
      prefix: --reference-gz-pac
  reference-gz-sa:
    type: ["null", File]
    inputBinding:
      position: 8
      prefix: --reference-gz-sa
  out:
    type: string
    inputBinding:
      position: 9
      prefix: --out
  regions:
    type: ["null", int]
    inputBinding:
      position: 10
      prefix: --regions
  threads:
    type: ["null", int]
    default: 2
    inputBinding:
      position: 11
      prefix: --threads
  normal-bai:
    type: ["null", File]
    inputBinding:
      position: 12
      prefix: --normal-bam-index
  tumor-bai:
    type: ["null", File]
    inputBinding:
      position: 13
      prefix: --tumor-bam-index
outputs:
  svabaout:
    type:
      type: array
      items: File
    outputBinding:
      glob: "svaba_out/*"


baseCommand: [/usr/bin/run_svaba]
