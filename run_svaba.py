#!/usr/bin/env python2.7
import argparse
import os
import re
import shlex
import logging
import subprocess
from distutils.dir_util import copy_tree

def prepare_tmp_dirs(tempdir, subdirs=['databases', 'data', 'runs']):
    if not os.path.isdir(tempdir):
        os.makedirs(tempdir)
    for subdir in subdirs:
        subdir_tmp = os.path.join('/tmp', subdir)
        subdir = os.path.join(tempdir, subdir)
        if not os.path.isdir(subdir):
            os.mkdir(subdir)
            if subdir_tmp != subdir:
                if not os.path.exists(subdir_tmp):
                    os.symlink(subdir, subdir_tmp)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--normal-bam', dest='normal',
                        help='Normal sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-t', '--tumor-bam', dest='tumor',
                        help='Tumor sorted and indexed BAM file',
                        required=True)
    parser.add_argument('--normal-bam-index',  dest='normal_bai',
                        help='Normal Bam file',  required=False)
    parser.add_argument('--tumor-bam-index',  dest='tumor_bai',
                        help='Tumor Bam file',  required=False)
    parser.add_argument('-G', '--reference-genome', dest='genome',
                        help='Bwa indexed reference genome file', required=True)
    parser.add_argument('-a', '--reference-gz-ann', dest='genome_ann',
                        help='', required=False)
    parser.add_argument('-b', '--reference-gz-amb', dest='genome_amb',
                        help='', required=False)
    parser.add_argument('-w', '--reference-gz-bwt', dest='genome_bwt',
                        help='', required=False)
    parser.add_argument('-c', '--reference-gz-pac', dest='genome_pac',
                        help='', required=False)
    parser.add_argument('-s', '--reference-gz-sa', dest='genome_sa',
                        help='', required=False)
    parser.add_argument('-o', '--out', dest='run_id',
                        help='Prefix and identifier of the run', required=True)
    parser.add_argument('-k', '--regions', dest='region',
                        help='Targeted intervals', required=False)
    parser.add_argument('-p', '--threads', dest='threads',
                        help=('NUM threads'),
                        default=2)
    parser.add_argument('--tmp', dest='tempdir',
                        help=('Temporary folder'),
                        default='/tmp')

    args = parser.parse_args()

    logging.basicConfig(format = '%(levelname)s : %(asctime)s : %(message)s',
        level=logging.INFO)

    tempdir = args.tempdir

    prepare_tmp_dirs(tempdir, ['databases', 'data', 'workdir'])

    output_dir = '/tmp/workdir'
    database_dir = '/tmp/databases'
    result_dir = os.getcwd()

    out_dir = os.path.join(output_dir, args.run_id)

    genome_name = os.path.basename(args.genome)
    genome_file = os.path.join(database_dir, genome_name)

    os.makedirs('svaba_out')
    faidx_needed = True
    # Join reference files to database_dir
    for key, val, in vars(args).iteritems():
        if re.match('genome', key):
            if val is None:
                logging.info('argument %s is missing: create BWA indexes' % key)
                os.symlink(args.genome, genome_file)
                idx_genome = ['bwa', 'index', genome_file]
                idx_genome = shlex.split(' '.join(map(str, idx_genome)))
                idx_genome_proc = subprocess.Popen(idx_genome)
                idx_genome_proc.communicate()[0]
                faidx_needed = False
                break
            else:
                file_names = os.path.basename(val)
                ref_files = os.path.join(database_dir, file_names)
                logging.info('Symlink %s to %s' % (val, ref_files))
                os.symlink(val, ref_files)

    if faidx_needed is True:
        idx_genome = ['samtools', 'faidx', genome_file]
        idx_genome = shlex.split(' '.join(map(str, idx_genome)))
        logging.info('Index genome fasta with: %s' % ' '.join(idx_genome))
        idx_genome_proc = subprocess.Popen(idx_genome)
        idx_genome_proc.communicate()[0]


    bam_path = '/tmp/data'
    tumor_link = os.path.join(bam_path, 'tumor.bam')
    normal_link = os.path.join(bam_path, 'normal.bam')
    tumor_bai_link = os.path.join(bam_path, 'tumor.bam.bai')
    normal_bai_link = os.path.join(bam_path, 'normal.bam.bai')
    logging.info('Symlink %s to %s' % (args.tumor, tumor_link))
    os.symlink(args.tumor, tumor_link)
    logging.info('Symlink %s to %s' % (args.normal, normal_link))
    os.symlink(args.normal, normal_link)
    idx_tumor = ['samtools', 'index', tumor_link]
    idx_normal = ['samtools', 'index', normal_link]

    idx_tumor = shlex.split(' '.join(map(str, idx_tumor)))
    idx_normal = shlex.split(' '.join(map(str, idx_normal)))


    if args.tumor_bai is None:
        logging.info('Tumor bai index file not found')
        logging.info('Index tumor bam with: %s' % ' '.join(idx_tumor))
        idx_tumor_proc = subprocess.Popen(idx_tumor)
        idx_tumor_proc.communicate()[0]
    else:
        logging.info('Symlink %s to %s' % (args.tumor_bai, tumor_bai_link))
        os.symlink(args.tumor_bai, tumor_bai_link)
    if args.normal_bai is None:
        logging.info('Normal bai index file not found')
        logging.info('Index nornal bam with: %s' % ' '.join(idx_normal))
        idx_normal_proc = subprocess.Popen(idx_normal)
        idx_normal_proc.communicate()[0]
    else:
        logging.info('Symlink %s to %s' % (args.normal_bai, normal_bai_link))
        os.symlink(args.normal_bai, normal_bai_link)

    svaba_cmd = ['svaba', 'run', '-t', tumor_link, '-n', normal_link, '-G', genome_file, '-a', out_dir, '-p', args.threads, '-z']
    if args.region is not None:
        svaba_cmd += ['-k', args.region]
    logging.info('Run svaba with: %s' % ' '.join(map(str, svaba_cmd)))
    svaba_cmd = shlex.split(' '.join(map(str, svaba_cmd)))
    svaba_proc = subprocess.Popen(svaba_cmd, stdout=subprocess.PIPE)
    out = svaba_proc.communicate()[0]
    svaba_dir = os.path.join(result_dir, 'svaba_out')
    logging.info('Copy results to: %s' % svaba_dir)
    copy_tree(output_dir, svaba_dir)

if __name__ == '__main__':
    main()
